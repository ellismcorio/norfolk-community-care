document.getElementById("nav_click").addEventListener("click", function() {
    document.getElementById("overlay").style.display = "flex";
    document.getElementById("body").style.overflow = "hidden";
    document.getElementById("nav").style.display = "flex";
    });
  
    document.getElementById("overlay").addEventListener("click", function() {
    document.getElementById("overlay").style.display = "none";
    document.getElementById("body").style.overflow = "scroll";
    document.getElementById("nav").style.display = "none";
    });
  
    document.getElementById("nav").addEventListener("click", function() {
    document.getElementById("overlay").style.display = "none";
    document.getElementById("body").style.overflow = "scroll";
    document.getElementById("nav").style.display = "none";
    });
  
    function scrollDetect(){
      var lastScroll = 0;
      window.onscroll = function() {
      let currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 0 && lastScroll <= currentScroll){
      lastScroll = currentScroll;
      document.getElementById("header").classList.remove("sticky");
      }else{
      lastScroll = currentScroll;
      document.getElementById("header").classList.add("sticky");
      }
    };
  }
  
    function topFunction() {

      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
  
  scrollDetect();